# OnyxOS
## About
Just some small kernel / bare metal shell made for fun.
## If your planning to contribute
### Make sure to read all the READMEs just so you know all the things you need to know all you need to know.

## What it has
1. A really basic shell.
2. A basic bootloader.
3. Minimal implementation of libc.

# Requirements for build
1. clang
2. nasm
3. lld or ld
4. dd
5. make
6. objcopy
7. QEMU or Bochs

# What it is meant to become
It is currently a basic OS that is built for BIOS and boots off floppy.
While I want to make it into a modern DOS system with POSIX compatibility.
Which means:
UEFI and BIOS compatible
Core implementation of MS-DOS/PC-DOS (R) compatibility
UNIX compatibility

# Stuff less important for people who will not be using(testing in a VM counts) or contributing this
---

# Rules for contribution
### I am reluctant on rules, but I have some standards.
1. No editor specific configuration unless fixes a linter issue
2. No adding a bunch of languages, while I support alternate languages, I think the project should be just C and Assembler/Assembly.
3. No annoying people for being inactive, they got their own lives.

# Goals

## Future goals
1. Memory FS. | partiality implemented
2. Basic text editor.
3. Programs if possible.
4. A better shell

## Current goals for software support
### Assemblers
2. nasm | Currently supported | Default
3. fasm
### Compilers
1. gcc
2. clang | Currently supported | Default
3. tcc
### Linkers
1. LD | Currently supported
2. LLD | Currently supported | Default
3. Mold
### Loaders
1. included one | only works on BIOS systems, will be default since for now | Default
2. GRUB | works with BIOS and EFI, and supports secure boot, most likely will be added
### Emulators
1. QEMU | currently used | Default
2. Bochs
### OS support
1. Linux | Currently supported
2. Windows | Don't expect it if I am alone on this project, I don't use Windows for dev
3. Mac OS | Someone else required, I have no Mac OS device
4. BSD | Probably supported, not tested
5. Anything else, since they will be very obscure, but most Unix based systems should work.

# Features that contributors should think about working on / doing
## Adding better support for their own OS
I myself will not add support for Windows or MacOS or other OSes, since I can not bring myself to use them.
## Refactoring or improving code
I don't know much assembly, so my assembly won't be great. Neither will my libc functions be great either, since I do it quickly so I can focus on the OS.
While it is not noticable at this small scale, it still helps to have it be well made so others don't have to suffer.
## More will be added if I get ideas, check in on this if you see edits to the readme.

# Known issues

1. Random boot error, it has not been known to cause anything. Nor is the reason known, if you have fixed it, please report how you did.
2. The shell has issues with how it spaces output. It is known to do this, but I do not know why, so I left it like that for now. Fix wanted.
3. Linters are buggy with the system, help of fixing linting issues is needed.

# Notes
1. I don't expect this to be big, nor even have any contribute. But if you can, and want to, please try to help, it could keep the project alive.
2. There will be mirrors on github and codeberg, possibly some other sites someday, so I can reach more people. But it is not gonna happen right now.
3. If you make a issue / pull, ping me on discord for a highest chance of me noticing. My @ is bumpscosity.
4. Please improve the readme, I suck at writing a readme, since I have trouble writing these. This one took be a bunch of time.
