#include <shell/keyboard.h>

#include "vga.h"

int x = 0;
int y = 24;

void kPrintChar(const char s) {
    unsigned short offset = (y * 80 + x) * 2;
    char* vga_buffer = (char*)0xB8000;
    vga_buffer[offset] = s;
    vga_buffer[offset + 1] = 0x07;

    x = x + 1;
    move_cursor(y, x-1);
}

void kprint(const char *s) {
    x = 0;
    
    while (*s != '\0') {
        if (*s == '\n')
            y++;
        kPrintChar(*s);
        s++;
    }

    y = y + 1;
}
