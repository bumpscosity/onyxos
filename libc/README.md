# libc | not full libc

## Just a little libc functions for use in the project due to static linking bloating the whole thing up.
### docs
Not much to explain, just use `#include <libc_header.h>` and then use it.
## Current headers
### string.h
Includes:
``` c
strcmp();
strncmp();
strlen();
```
### typedef.h
A bunch of common names for existing types.
