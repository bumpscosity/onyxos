#include <string.h>

int strcmp(const char* str1, const char* str2) {
    while (*str1 && (*str1 == *str2)) {
        str1++;
        str2++;
    }

    return *(unsigned char*)str1 - *(unsigned char*)str2;
}

int strncmp(const char* str1, const char* str2, unsigned int num) {
    for (unsigned int i = 0; i < num; i++) {
        if (str1[i] != str2[i])
            return str1[i] - str2[i];
        if (str1[i] == '\0')
            return 0;
    }
    return 0;
}

unsigned long strlen(const char *str) {
    unsigned long len = 0;
    while (*str != '\0') {
        len++;
        str++;
    }
    return len;
}


void reverseString(char* str, int length) {
    int start = 0;
    int end = length - 1;
    while (start < end) {
        char temp = str[start];
        str[start] = str[end];
        str[end] = temp;
        start++;
        end--;
    }
}

char* ansi(int num) {
    char *str = "\0";
    int index = 0;
    int temp;

    if (num == 0) {
        str[index++] = '0';
        str[index] = '\0';
        return str;
    }

    if (num < 0) {
        str[index++] = '-';
        temp = -num;
    } else {
        temp = num;
    }

    while (temp != 0) {
        str[index++] = '0' + (temp % 10);
        temp /= 10;
    }

    str[index] = '\0';

    if (num < 0) {
        reverseString(str + 1, index - 1);
    } else {
        reverseString(str, index);
    }

    return str;
}

char* strcat(char* dest, const char* src) {
    char* d = dest;
    const char* s = src;

    // Move d to the end of the destination string
    while (*d)
        d++;

    // Copy the source string to the destination
    while ((*d++ = *s++))
        ;

    return dest;
}
