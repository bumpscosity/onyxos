#pragma once

int strcmp(const char* str1, const char* str2);
int strncmp(const char* str1, const char* str2, unsigned int num);
unsigned long strlen(const char *str);
void reverseString(char* str, int length);
char *ansi(int num);
char* strcat(char* dest, const char* src);
