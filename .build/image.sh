#!/bin/bash

bios=$1
as=$2

if [ "$bios" -eq 1 ]; then
    "$as" boot/boot.s -f bin -o bin/boot.bin -i boot
	objcopy -O binary bin/kernel.elf bin/kernel.bin
	cat bin/boot.bin bin/kernel.bin > bin/os.bin
	dd if=/dev/zero of=os.img bs=512 count=2880
	dd if=bin/os.bin of=os.img conv=notrunc
    echo "$as" boot/boot.s -f bin -o bin/boot.bin -i boot
	echo objcopy -O binary bin/kernel.elf bin/kernel.bin
	echo cat bin/boot.bin bin/kernel.bin > bin/os.bin
	echo dd if=/dev/zero of=os.img bs=512 count=2880
	echo dd if=bin/os.bin of=os.img conv=notrunc
	echo qemu-system-x86_64 -machine pc -k en-us -drive format=raw,file=os.img,index=0,if=floppy -m 512M
	qemu-system-x86_64 -machine pc -k en-us -drive format=raw,file=os.img,index=0,if=floppy -m 512M	
else
    mv bin/kernel.elf iso/boot/kernel.elf
    grub-mkrescue -o os.iso iso
    qemu-system-x86_64 -machine pc -k en-us -cdrom os.iso -m 512M
fi
