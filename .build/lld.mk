# .build/lld.mk
# configuration for linking with lld

LD = ld.lld
LDFLAGS = -static -nostdlib -L./libc/headers/ -L./
LDPRIORITY = bin/kernel_entry.o bin/kernel.o bin/portio.o

LD_COMMAND = $(LD) $(LDFLAGS) -melf_i386 -o iso/boot/kernel.elf -Ttext 0x7ef0 $(LDPRIORITY) --start-group $(filter-out $(wildcard $(LDPRIORITY)), $(wildcard */*.o)) --end-group
