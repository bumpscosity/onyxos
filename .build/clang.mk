# .build/clang.mk
# configuration for building with clang
# the extra checks can be removed, but it is not advised.

CC = clang
PROC_FLAGS = -Wall -Wextra -Werror -pedantic -std=c11
FLAGS = -ffreestanding -m32 -fno-exceptions -fno-stack-protector $(PROC_FLAGS) -I./libc/headers/ -I./
