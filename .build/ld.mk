# .build/ld.mk
# configuration for linking with ld

LD = ld
LDFLAGS = --entry kmain -m elf_i386 -static -nostdlib -L./libc/headers/ -L./
LDPRIORITY = bin/kernel_entry.o bin/kernel.o bin/portio.o

LD_COMMAND = $(LD) $(LDFLAGS) -o bin/kernel.elf -Ttext 0x7ef0 $(LDPRIORITY) --start-group $(filter-out $(wildcard $(LDPRIORITY)), $(wildcard */*.o)) --end-group --oformat elf32-i386

