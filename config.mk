# config.mk

# Loader config
# Set to 1 for included loader, set to 0 for GRUB
BIOS = 0

# Build config
include .build/clang.mk
include .build/lld.mk
include .build/nasm.mk
