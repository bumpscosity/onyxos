# makefile

include config.mk

## C source files
CSRC := $(shell find ./ -name "*.c")

## C target files
CTAR := $(patsubst %.c,%.o,$(CSRC))

## ASM source files
ASMSRC := $(shell find ./ -name "*.asm")

## ASM target files
ASMTAR := $(patsubst %.asm,%.o,$(ASMSRC))

all: prebuild build

# Clean old binaries
prebuild:	## Prebuild instructions
	clear
	rm -rf bin
	mkdir bin
	rm -rf iso
	cp -r .build/iso iso

build: $(ASMTAR) $(CTAR)
	# Link kernel | command in .build/$yourlinker.mk
	$(LD_COMMAND)
	@.build/image.sh $(BIOS) $(AS)

# Build the C parts of the kernel
%.o: %.c
	$(CC) $(FLAGS) -c $< -o bin/$(@F)

# Build the assembly parts of the kernel
%.o: %.asm
	$(AS) $(ASFLAGS) $< -o bin/$(@F)

