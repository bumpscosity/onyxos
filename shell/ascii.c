#include "keyboard.h"

char ascii_map[] = {
    0,   0, '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', 0,   0, // scan codes 0-15
    'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n', 0,   'a', 's', // scan codes 16-29
    'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', '`', 0,   '\\', 'z', 'x', 'c', 'v', // scan codes 30-44
    'b', 'n', 'm', ',', '.', '/', 0,   '*', 0, ' ', 0 // scan codes 45-57
};

int ascii_map_size = sizeof(ascii_map) / sizeof(ascii_map[0]);


