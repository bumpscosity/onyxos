#include <fs/fs.h>

#include "commands.h"

void processTouchCommand(void) {
    unsigned int address = 0;
    int foundAddress = 0;

    for (int i = 0; command[i] != '\0'; i++) {
        if (foundAddress) {
            // Parse the address
            if (command[i] >= '0' && command[i] <= '9') {
                address = address * 16 + (command[i] - '0');
            } else if (command[i] >= 'a' && command[i] <= 'f') {
                address = address * 16 + (command[i] - 'a' + 10);
            } else if (command[i] >= 'A' && command[i] <= 'F') {
                address = address * 16 + (command[i] - 'A' + 10);
            } else {
                break; // Stop if we encounter a non-hex character
            }
        } else if (command[i] == 'x' && (command[i + 1] == '0' || command[i + 1] == 'X')) {
            foundAddress = 1;
        }
    }

    if (foundAddress) {
        // Successfully extracted the address, now call createFile
        createFile(address);
    } else {
        kprintE("Invalid touch command. Use format: touch 0xXXXXX");
    }
}
