#include "commands.h"

void help(void) {
    kprintE("test               | Outputs \"Test success!\", if it does not, something is wrong");
    kprintE("clear / cls        | Clears the buffer"); 
    kprintE("echo <text>        | Outputs the text input to the buffer");
    kprintE("reboot             | Soft reboots the computer");
    kprintE("bootlog            | Outputs any messages made during boot");
    kprintE("touch 0xXXXXX      | Creates a empty file at the specified location");
    kprintE("cat 0xXXXXX        | Reads a file at the specified location");
    kprintE("put 0xXXXXX <text> | Writes the text entered to a file");
}
