#pragma once

extern char command[256];
extern int pos;
extern int cx;
extern int cy;

void kPrintCharE(const char s);
void kprintE(const char *s); // E means exact, so it will use cx and cy
void kprintSE(const char *s, int x, int y); // SE means super exact, it will use defined values instead

void process_command(void);

// Commands
void echo(void);
void clear(void);
void processTouchCommand(void);
void processCatCommand(void);
void processPutCommand(void);
void help(void);
