#include <fs/fs.h>

#include "commands.h"

void processPutCommand(void) {
    unsigned int address = 0;
    int foundAddress = 0;
    int spaceIndex = -1;

    for (int i = 0; command[i] != '\0'; i++) {
        if (foundAddress) {
            // Parse the address
            if (command[i] >= '0' && command[i] <= '9') {
                address = address * 16 + (command[i] - '0');
            } else if (command[i] >= 'a' && command[i] <= 'f') {
                address = address * 16 + (command[i] - 'a' + 10);
            } else if (command[i] >= 'A' && command[i] <= 'F') {
                address = address * 16 + (command[i] - 'A' + 10);
            } else {
                break; // Stop if we encounter a non-hex character
            }
        } else if (command[i] == 'x' && (command[i + 1] == '0' || command[i + 1] == 'X')) {
            foundAddress = 1;
        } else if (command[i] == ' ' && spaceIndex == -1) {
            spaceIndex = i;
        }
    }

    if (foundAddress && spaceIndex != -1) {
        // Successfully extracted the address and space index
        char *text = command + spaceIndex + 1; // Point to the character after the space
        writeFile(address, text);
    } else {
        kprintE("Invalid put command. Use format: put 0xXXXXX <text>");
    }
}

