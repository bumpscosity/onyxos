#include "commands.h"

static char* vga_buffer = (char*)0xB8000;
static int offset;

void clear(void) {
    offset = (0 * 80 + 0) * 2;

    for (int i = 0; i < 80*24; i++) {
        vga_buffer[offset + (i * 2)] = ' ';
        vga_buffer[offset + (i * 2) + 1] = 0x07;
    } 
    
    cy = 0;
    cx = 0;
}
