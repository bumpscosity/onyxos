#include "commands.h"

void kPrintCharE(const char s) {
    unsigned short offset = (cy * 80 + cx) * 2;
    char* vga_buffer = (char*)0xB8000;
    vga_buffer[offset] = s;
    vga_buffer[offset + 1] = 0x07;

    cx = cx + 1;
}

void kprintE(const char *s) {
    cx = 0;

    while (*s != '\0') {
        kPrintCharE(*s);
        s++;
    }

    while (cx < 80) {
        kPrintCharE(' '); // Clear any remaining characters from the previous output
    }

    cy++;
    if (cy >= 22) {
        // Scroll the screen up by one line
        for (int i = 0; i < 23 * 80; i++) {
            char* vga_buffer = (char*)0xB8000;
            vga_buffer[i * 2] = vga_buffer[(i + 80) * 2];
            vga_buffer[i * 2 + 1] = vga_buffer[(i + 80) * 2 + 1];
        }
        cy = 22; // Reset cy to the last row
    }
}
