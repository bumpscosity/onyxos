#include <string.h>
#include <onyx/power.h>

#include <fs/fs.h>

#include "commands.h"

void process_command(void) {
    switch(command[0]) {
        case 'c':
            if (strncmp(command, "clear", 5) == 0) {
                clear(); break;}
            if (strncmp(command, "cls", 3) == 0) {
                clear(); break;}
            if (strncmp(command, "cat", 3) == 0) {
                processCatCommand(); break;}
        case 't':
            if (strncmp(command, "test", 4) == 0) {
                kprintE("Test success!"); break;}
            if (strncmp(command, "touch", 5) == 0) {
                processTouchCommand(); break;}
        case 's':
            if (strncmp(command, "shutdown", 8) == 0) {
                clear(); 
                kprintE("The system can now be powered off safely");
                shutdown(); break;}
        case 'e':
            if (strncmp(command, "echo", 4) == 0) {
                echo(); break;}
        case 'p':
            if (strncmp(command, "put", 3) == 0) {
                processPutCommand(); break;}
        case 'h':
            if (strncmp(command, "help", 4) == 0) {
                help(); break;}
        case 'r':
            if (strcmp(command, "reboot") == 0) {
                reboot(); break;}
        default:
            kprintE("The command you used was not recognized");
            break;
    }
    for (int i = 0; i < 256; i++) {
        command[i] = '\0';
    }
}
