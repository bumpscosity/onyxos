#include "commands.h"

static void kPrintCharSE(const char s, int x, int y) {
    unsigned short offset = (y * 80 + x) * 2;
    char* vga_buffer = (char*)0xB8000;
    vga_buffer[offset] = s;
    vga_buffer[offset + 1] = 0x07;
}

void kprintSE(const char *s, int x, int y) {
    while (*s != '\0') {
        if (*s == '\n') {
            x = 0; y++;}
        kPrintCharSE(*s, x, y);
        s++;
        x++;
    }
}
