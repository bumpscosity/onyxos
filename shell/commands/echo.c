#include "commands.h"

void echo(void) {
    int space_index = -1;
    for (int i = 0; command[i] != '\0'; i++) {
        if (command[i] == ' ') {
            space_index = i;
            break;
        }
    }

    if (space_index != -1) {
        const char *output = command + space_index + 1; // Point to the character after the space
        kprintE(output); // Print to command history area
    }
}
