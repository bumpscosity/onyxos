# Shell

## This handles the keyboard, and the command shell

# Commands

## Current commands
1. `help` Provides help with commands.
2. `test` Prints a test message.
3. `echo <text>` Prints text.
4. `clear / cls` Clears the screen.
5. `reboot` Reboots the computer.
6. `bootlog` Prints any boot errors, will tell if there are no errors.
8. FS related commands can be found in fs/README.md
