#include <string.h>

#include <onyx/portio.h>
#include <shell/keyboard.h>
#include <shell/commands/commands.h>
#include <screen/vga.h>

void shell(void) {
    unsigned char key;
    kprint("> "); x++; y--; move_cursor(y, x);
    while (1) {
        key = getch();
        switch(key) {
            case BACKSPACE:
            if (x > 0) { // make sure there is a character to delete
                x--; // move back to the previous column
                unsigned short offset = (y * 80 + x) * 2;
                char* vga_buffer = (char*)0xB8000;
                vga_buffer[offset] = ' ';
                vga_buffer[offset + 1] = 0x07;
                move_cursor(y, x);
                command[pos] = '\0';
                pos--;
            }
            break;

            case LEFT_ARROW:
            left_arrow();
            break;
            case RIGHT_ARROW:
            right_arrow();
            break;

            case ENTER:
            x = 0;
            pos = 0;
            move_cursor(y, x);
            process_command();
            x = 0;
            move_cursor(y, x);
            for (int i = 0; i < 80; i++) { char* vga_buffer = (char*)0xB8000; unsigned short offset = (24 * 80 + i) * 2; vga_buffer[offset] = ' ';}
            kprint("> "); x++; y--; move_cursor(y, x);
            break;

            default:
            if (key < ascii_map_size) {
                char ascii = ascii_map[key];
                kPrintChar(ascii);
                command[pos] = ascii;
                pos++;     
            }  
            break;
        }
    }
}
