#pragma once

#define BACKSPACE 0x0E
#define LEFT_ARROW 0x4B
#define RIGHT_ARROW 0x4D
#define UP_ARROW 0x48
#define DOWN_ARROW 0x50
#define ENTER 0x1C

void move_cursor(int row, int col);
void right_arrow(void);
void left_arrow(void);
unsigned char getch(void);

extern char ascii_map[]; 

extern int ascii_map_size;

extern int x;
extern int y;
