#include <onyx/portio.h>
#include <shell/commands/commands.h>

#include "keyboard.h"

void move_cursor(int row, int col) {
    unsigned short position = (row * 80) + col;

    outb(0x3D4, 0x0F);
    outb(0x3D5, (unsigned char)(position & 0xFF));
    outb(0x3D4, 0x0E);
    outb(0x3D5, (unsigned char)((position >> 8) & 0xFF));
}


void right_arrow(void) {
    if (y < 80 - x) {
        x++;
        move_cursor(y, x);
        pos++;
    }
}

void left_arrow(void) {
    if (x > 0) { // make sure there is a character to move back to
        x--;
        move_cursor(y, x);
        pos--;
    }
}

unsigned char getch(void) {
    unsigned char key;
    while (1) {
        __asm__("inb $0x64, %0" : "=a" (key));
        if (key & 0x01) { // check bit 0 of the status byte to see if a key has been pressed
            __asm__("inb $0x60, %0" : "=a" (key));
            return key;
        }
    }
}


