#include <shell/keyboard.h>
#include <shell/shell.h>

char command[256];
int pos = 0;
int cx = 0;
int cy = 0;

static char* bootlogVar = (char*)0x1200000; 
char* vga = (char*)0xB8000;

void kmain(void) {
    // Logging any errors presented by the loader
    for (int i = 0; i < 80; i++) {
        bootlogVar[i] = vga[i * 2]; // Copy character
    }
    bootlogVar[80] = '\0'; // Null-terminate the string
    // Clearing the screen and entering the shell
    clear();
    shell();
    return;
}
