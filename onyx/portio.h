#pragma once

extern unsigned char inb(unsigned short port);
extern void outb(unsigned short port, unsigned char data);
extern unsigned short inw(unsigned short port);
extern void outw(unsigned short port, unsigned short data);
extern unsigned int inl(unsigned short port);
extern void outl(unsigned short port, unsigned int data);
