# onyx directory | kernel/misc stuff

## Docs
### general stuff
Use `#include <onyx/example.h> ` for including headers, ignore any errors with it not being found, it is currently a issue.
### portio.h
Use `#include <onyx/portio.h>` to include the header.
Then you can use `inb; inw; inl` and `outb; outw; inw` to send data to a port, and receive data from a port.
### power.h
Use `#include <onyx/power.h>` to include the header.
Then you can run `reboot()` to reboot the system, this is currently the only function in this header.
