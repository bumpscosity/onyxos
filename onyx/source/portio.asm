section .text
    global inb
    global outb
    global inw
    global outw
    global inl
    global outl

inb:
    mov dx, [esp+4]    ; Load port number from stack
    in al, dx          ; Read a byte from the port
    ret

outb:
    mov dx, [esp+4]    ; Load port number from stack
    mov al, [esp+8]    ; Load data from stack
    out dx, al         ; Write a byte to the port
    ret

inw:
    mov dx, [esp+4]    ; Load port number from stack
    in ax, dx          ; Read a word from the port
    ret

outw:
    mov dx, [esp+4]    ; Load port number from stack
    mov ax, [esp+8]    ; Load data from stack
    out dx, ax         ; Write a word to the port
    ret

inl:
    mov dx, [esp+4]    ; Load port number from stack
    in eax, dx         ; Read a long from the port
    ret

outl:
    mov dx, [esp+4]    ; Load port number from stack
    mov eax, [esp+8]   ; Load data from stack
    out dx, eax        ; Write a long to the port
    ret


