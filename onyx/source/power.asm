section .text
global reboot
global shutdown

reboot:
    cli                 ; Disable interrupts

    ; Clear all keyboard buffers (output and command buffers)
.clear_buffers:
    in al, 0x64         ; Read the status register
    test al, 1          ; Check if the output buffer is full
    jnz .clear_buffers  ; If full, keep reading

    in al, 0x60         ; Empty the keyboard data register

    in al, 0x64         ; Read the status register again
    test al, 2          ; Check if the command buffer is full
    jnz .clear_buffers  ; If full, keep reading

    in al, 0x60         ; Empty the keyboard command register

    ; Pulse CPU reset line
    mov al, 0xFE        ; Load the reset command
    out 0x64, al        ; Send reset command to the keyboard controller

halt:
    hlt                 ; Halt the CPU
    jmp halt            ; If a NMI is received, halt again

shutdown:
    cli
    hlt
