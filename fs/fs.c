#include <shell/shell.h>

#include "fs.h"

static unsigned int upsizeMemory(unsigned int lowLevelAddress) {
    // Assuming low-level addresses are 20 bits wide
    // and you want to convert them to a 32-bit address space
    unsigned int highLevelAddress = lowLevelAddress | 0x10000000; // Set the higher bits

    return highLevelAddress;
}

extern void writeToFile(unsigned int address, char value);

void createFile(unsigned int address) {
    unsigned int newAddress = upsizeMemory(address);
    for (int i = 0; i < 2000; i++) {
        writeToFile(newAddress, '\0');
        newAddress++;
    }
}

void writeFile(unsigned int address, char* string) {
    unsigned int newAddress = upsizeMemory(address);

    for (int i = 0; string[i] != '\0'; i++) {
        writeToFile(newAddress, string[i]);
        newAddress++;
    }
    writeToFile(newAddress, '\0');
}

void readFile(unsigned int address) {
    unsigned int newAddress = upsizeMemory(address);
    char *file = (char*)newAddress;
    kprintE(file);
}
