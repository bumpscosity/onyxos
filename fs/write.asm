global writeToFile

section .text
writeToFile:
    mov eax, [esp+4]  ; Load the address into eax
    mov cl, [esp+8]   ; Load the value into cl
    mov [eax], cl     ; Store the value at the address

    ret
