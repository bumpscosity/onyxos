# Memory File System (MFS)

## How to use

### Creating a file
You can use `touch` to create a file. The command format is `touch 0xXXXXX`.
Example
``` sh
touch 0x02000
```
It is recommended for files to have a 2000 byte gap to prevent overwriting.

### Reading a file
You can use `cat` to read a file. The command format is `cat 0xXXXXX`.
Example
``` sh
cat 0x02000
```

### Writing to a file
You can use `put` to write to a file. The command format is `put 0xXXXXX <text>`.
Example
``` sh
put 0x02000 This is a message
```

### Using all you learnt
This is a example of using MFS to create, write, read, a file.
``` sh
touch 0x02000
put 0x02000 This is a message
cat 0x02000
```
Expected output:
``` sh
0x02000 This is a message
```

## About MFS

### Pros:
1. Saves across soft reboots.
2. Can be reset via cold reboots.
3. Extremely simple to implement.
4. No need to write to disk.
5. Works from a live environment

### Cons:
1. Doesn't save across cold reboots
2. Doesn't write to disk.
3. Is limited to 2000 bytes currently, but can be changed.
4. Can't use disk so less storage
5. No directories
6. No filenames
